This repository contains a list of every word which is not used in what3words,
as referenced against this dictionary.
https://github.com/dwyl/english-words/blob/7cb484da5de560c11109c8f3925565966015e5a9/words_alpha.txt?raw=true
